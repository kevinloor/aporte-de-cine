﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cineevaluacion
{
    class Usuario
    {
         public string nombre { get; set; }
        public string peliculaescogida { get; set; }
        public string saladecine { get; set; }
        public int asiento { get; set; }
       

        public Usuario(string nombre, string peliculaescogida, string saladecine, int asiento )
        {
            this.nombre = nombre;
            this.peliculaescogida = peliculaescogida;
            this.saladecine = saladecine;
            this.asiento = 2;

        }
        public string getnombre()
        {
            return nombre;
        }

        public void setnombre(string nombre)
        {
            this.nombre = nombre;
        }

        public string getpeliculaescogida()
        {
            return peliculaescogida;
        }

        public void setpeliculaescogida(string peliculaescogida)
        {
            this.peliculaescogida = peliculaescogida;
        }

        public string getsaladecine()
        {
            return saladecine;
        }

        public void setsaladecine(string saladecine)
        {
            this.saladecine = saladecine;
        }

        public int getasiento()
        {
            return asiento;
        }

        public void setasiento(int asiento)
        {

            if (asiento == 2)
            {
                this.asiento = asiento;
            }
            else
            {
                Console.WriteLine("asientos disponibles");
            }

               
        }
    }
}
