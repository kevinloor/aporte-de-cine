﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cineevaluacion
{
     public class stockdulce 
    {
        public string hotdog {get; set;}
        public string nachos { get; set;}
        public string gomitas { get; set; }
        public string cola { get; set; }
        public string chocolate { get; set; }
        public string canguil { get; set; }
        public string papas { get; set; }
        public string chicles { get; set; }


        public stockdulce(string hotdog, string nachos )
        {
            this.hotdog = hotdog;
            this.nachos = nachos;
        }

        public stockdulce(string hotdog, string nachos, string gomitas, string cola)
        {
            this.gomitas = gomitas;
            this.cola = cola;
        }

        public stockdulce(string hotdog, string nachos, string gomitas, string cola, string chocolate, string canguil)
        {
            this.chocolate = chocolate;
            this.canguil = canguil;
        }

        public string gethotdog()
        {
            return hotdog;
        }

        public void sethotdog( string hotdog)
        {
            this.hotdog = hotdog; 
        }
    }
}
